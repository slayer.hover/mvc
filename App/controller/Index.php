<?php

namespace app\controller;

use app\controller\Core;
use lib\Cache;
use Payment\Pay;
use Payment\Notify;
use lib\Validate;
use Illuminate\Database\Capsule\Manager as DB;

class Index extends Core
{
    public function index()
    {
        $this->_view->display('index/index.html');
    }

    public function goods()
    {
        $this->_view->assign('id', $this->get('id', 0));
        $this->_view->display('index/goods.html');
    }

    public function success()
    {
        $this->_view->display('index/success.html');
    }

    public function disp()
    {
        //$this->_view->setPath('/App/view/'.$this->action.'.html');
        $this->_view->assign('customer', DB::table('customer')->get());
        $this->_view->display('index/disp.html');
    }

    public function spring()
    {
        json(['data'=>DB::table('customer')->get()]);
    }

}

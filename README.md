# php微MVC框架


#### 介绍
php微MVC框架示例

#### 软件架构
运行环境要求PHP7.3+。


#### 安装教程
入口文件： public/index.php


#### nginx rewrite配置
```shell
location / {
   if (!-e $request_filename) {
   		rewrite  ^(.*)$  /index.php/$1  last;
    }
}
```

#### apache rewrite配置
```shell
<IfModule mod_rewrite.c>
  Options +FollowSymlinks -Multiviews
  RewriteEngine On

  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ index.php/$1 [QSA,PT,L]
</IfModule>
```

#### 结构目录
```shell
www  WEB部署目录
├─App           应用目录
│  ├─controller         控制器目录
│  ├─model        		模型目录
│  └─view               视图目录
│
├─includes              配置目录
│  ├─config.php         应用配置
│  └─function.php       全局函数
│
├─library               库目录
│  ├─application.php    应用主类
│  └─...                更多
│
├─public                WEB目录（对外访问目录）
│  ├─index.php          入口文件
│  └─nginx.htaccess     用于nginx的重写
│
├─logs              	日志目录
│  └─...	            框架LOG文件
│
├─vendor                第三方类库目录（Composer依赖库）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
```

<?php
header('content-type:text/html;charset=utf-8');
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('PRC');

define('ROOT_DIR', dirname(__FILE__) . '/../');
define('LOG_DIR', ROOT_DIR . '/logs/');
define('CACHE_KEY_PREFIX', '');

require(ROOT_DIR . '/vendor/autoload.php');
require(ROOT_DIR . '/includes/config.php');
require(ROOT_DIR . '/includes/function.php');

use lib\Container;
use lib\Log;
try {
    Container::get('Application')->run();
}catch (\Throwable $e){
    Log::out('[异常处理]' . $e->getMessage());
}

<?php

namespace lib;

class Container
{
    protected static $instance;
    protected $container = [];

    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    public function bind($name, $instance)
    {
        $this->container[$name] = $instance;
    }

    public function make($name, $vars=[])
    {
        if (!isset($this->container[$name])) {
            if($name==='Application'){
                $instance = 'lib\\Application';
            }else {
                $instance = 'app\\controller\\' . $name;
            }
            $instance = new $instance($vars);
            $this->bind($name, $instance);
        }
        return $this->container[$name];
    }

    public static function get($abstract, $vars=[])
    {
        return static::getInstance()->make($abstract, $vars);
    }

    public static function remove($abstract)
    {
        return static::getInstance()->delete($abstract);
    }

    public static function clear()
    {
        return static::getInstance()->flush();
    }

    public function flush()
    {
        $this->container = [];
    }

}

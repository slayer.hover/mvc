<?php
namespace lib;

class View
{
    private $path = '';
    private $dataset=[];

    public function setPath($path){
        $this->path = $path;
    }

    public function assign($key, $value)
    {
        $this->dataset[$key] = $value;
    }

    public function display($path = '')
    {
        if($path==''){$path = $this->path;}
        $path = ROOT_DIR . 'App/view/' . $path;
        if (file_exists($path) && is_readable($path)) {
            foreach ($this->dataset as $k=>$v){
                $$k = $v;
            }
            require $path;
        }
        return false;
    }

}
